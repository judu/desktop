Title: app-misc/AusweisApp2 has been renamed to app-misc/AusweisApp
Author: Timo Gurr <tgurr@exherbo.org>
Content-Type: text/plain
Posted: 2023-11-09
Revision: 1
News-Item-Format: 1.0
Display-If-Installed: app-misc/AusweisApp2

Please install app-misc/AusweisApp and *afterwards* uninstall app-misc/AusweisApp2.

1. Take note of any packages depending on app-misc/AusweisApp2:
cave resolve \!app-misc/AusweisApp2

2. Install app-misc/AusweisApp:
cave resolve app-misc/AusweisApp -x

3. Re-install the packages from step 1.

4. Uninstall app-misc/AusweisApp2:
cave resolve \!app-misc/AusweisApp2 -x

Do it in *this* order or you'll potentially *break* your system.
