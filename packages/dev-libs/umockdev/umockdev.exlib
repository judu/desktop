# Copyright 2021 Florentin Dubois <florentin.dubois@hey.com>
# Distributed under the terms of the GNU General Public License v2

require github [ user="martinpitt" suffix="tar.xz" release=${PV} ]
require vala [ vala_dep=true ]
require meson
require python [ blacklist=2 multibuild=false ]

export_exlib_phases pkg_setup src_prepare src_test

SUMMARY="Mock hardware devices for creating unit tests"

LICENCES="LGPL-2.1"
SLOT="0"
MYOPTIONS="
    ( providers: eudev systemd ) [[ number-selected = exactly-one ]]
"

# tests run lsusb and sydbox prevents access to the connected hardware
RESTRICT="test"

DEPENDENCIES="
    build+run:
        app-admin/chrpath
        dev-libs/glib:2[>=2.32.0]
        dev-libs/libpcap
        gnome-desktop/libgudev
        providers:eudev? ( sys-apps/eudev )
        providers:systemd? ( sys-apps/systemd )
    test:
        gnome-bindings/pygobject:3[python_abis:*(-)?]
        gnome-desktop/libgudev[gobject-introspection]
"

umockdev_pkg_setup() {
    meson_pkg_setup
    vala_pkg_setup
}

umockdev_src_prepare() {
    meson_src_prepare

    # respect selected python abi
    edo sed \
        -e "s:python3:python$(python_get_abi):g" \
        -i tests/test-umockdev.py
}

umockdev_src_test() {
    esandbox allow_net "unix:/tmp/umockdev_test"

    esandbox allow_net "unix:${TEMP%/}/umockdev.*/*"
    esandbox allow_net "unix:${TEMP%/}/umockdev.*/ioctl/_default"

    esandbox allow_net "unix:${WORK%/}/*/ioctl/_default"
    esandbox allow_net --connect "unix:${WORK%/}/*/ioctl/_default"
    esandbox allow_net --bind "unix:${WORK%/}/*/ioctl/_default"

    esandbox allow_net --bind "unix:${WORK%/}/umockdev.*/dev/socket/*"
    esandbox allow_net --bind "unix:${TEMP%/}/umockdev.*/dev/socket/*"
    esandbox allow_net --connect "unix:${WORK%/}/umockdev.*/dev/socket/*"
    esandbox allow_net --connect "unix:${TEMP%/}/umockdev.*/dev/socket/*"

    esandbox allow_net --bind "unix:${WORK%/}/umockdev.*/dev/*"
    esandbox allow_net --bind "unix:${TEMP%/}/umockdev.*/dev/*"
    esandbox allow_net --connect "unix:${WORK%/}/umockdev.*/dev/*"
    esandbox allow_net --connect "unix:${TEMP%/}/umockdev.*/dev/*"

    esandbox allow_net --bind "unix:${WORK%/}/umockdev.*/dev/bus/usb/*/*"
    esandbox allow_net --bind "unix:${TEMP%/}/umockdev.*/dev/bus/usb/*/*"
    esandbox allow_net --connect "unix:${WORK%/}/umockdev.*/dev/bus/usb/*/*"
    esandbox allow_net --connect "unix:${TEMP%/}/umockdev.*/dev/bus/usb/*/*"

    esandbox allow_net --bind "unix:${WORK%/}/umockdev.*/ioctl/dev/bus/usb/*/*"
    esandbox allow_net --bind "unix:${TEMP%/}/umockdev.*/ioctl/dev/bus/usb/*/*"
    esandbox allow_net --connect "unix:${WORK%/}/umockdev.*/ioctl/dev/bus/usb/*/*"
    esandbox allow_net --connect "unix:${TEMP%/}/umockdev.*/ioctl/dev/bus/usb/*/*"

    esandbox allow_net --bind "unix:${WORK%/}/umockdev.*/ioctl/_default"
    esandbox allow_net --bind "unix:${TEMP%/}/umockdev.*/ioctl/_default"
    esandbox allow_net --connect "unix:${WORK%/}/umockdev.*/ioctl/_default"
    esandbox allow_net --connect "unix:${TEMP%/}/umockdev.*/ioctl/_default"

    esandbox allow_net --bind "unix:${WORK%/}/umockdev.*/ioctl/dev/*"
    esandbox allow_net --bind "unix:${TEMP%/}/umockdev.*/ioctl/dev/*"
    esandbox allow_net --connect "unix:${WORK%/}/umockdev.*/ioctl/dev/*"
    esandbox allow_net --connect "unix:${TEMP%/}/umockdev.*/ioctl/dev/*"

    meson_src_test

    esandbox disallow_net "unix:/tmp/umockdev_test"

    esandbox disallow_net "unix:${TEMP%/}/umockdev.*/*"
    esandbox disallow_net "unix:${TEMP%/}/umockdev.*/ioctl/_default"

    esandbox disallow_net "unix:${WORK%/}/*/ioctl/_default"
    esandbox disallow_net --connect "unix:${WORK%/}/*/ioctl/_default"
    esandbox disallow_net --bind "unix:${WORK%/}/*/ioctl/_default"

    esandbox disallow_net --bind "unix:${WORK%/}/umockdev.*/dev/socket/*"
    esandbox disallow_net --bind "unix:${TEMP%/}/umockdev.*/dev/socket/*"
    esandbox disallow_net --connect "unix:${WORK%/}/umockdev.*/dev/socket/*"
    esandbox disallow_net --connect "unix:${TEMP%/}/umockdev.*/dev/socket/*"

    esandbox disallow_net --bind "unix:${WORK%/}/umockdev.*/dev/*"
    esandbox disallow_net --bind "unix:${TEMP%/}/umockdev.*/dev/*"
    esandbox disallow_net --connect "unix:${WORK%/}/umockdev.*/dev/*"
    esandbox disallow_net --connect "unix:${TEMP%/}/umockdev.*/dev/*"

    esandbox disallow_net --bind "unix:${WORK%/}/umockdev.*/dev/bus/usb/*/*"
    esandbox disallow_net --bind "unix:${TEMP%/}/umockdev.*/dev/bus/usb/*/*"
    esandbox disallow_net --connect "unix:${WORK%/}/umockdev.*/dev/bus/usb/*/*"
    esandbox disallow_net --connect "unix:${TEMP%/}/umockdev.*/dev/bus/usb/*/*"

    esandbox disallow_net --bind "unix:${WORK%/}/umockdev.*/ioctl/dev/bus/usb/*/*"
    esandbox disallow_net --bind "unix:${TEMP%/}/umockdev.*/ioctl/dev/bus/usb/*/*"
    esandbox disallow_net --connect "unix:${WORK%/}/umockdev.*/ioctl/dev/bus/usb/*/*"
    esandbox disallow_net --connect "unix:${TEMP%/}/umockdev.*/ioctl/dev/bus/usb/*/*"

    esandbox disallow_net --bind "unix:${WORK%/}/umockdev.*/ioctl/_default"
    esandbox disallow_net --bind "unix:${TEMP%/}/umockdev.*/ioctl/_default"
    esandbox disallow_net --connect "unix:${WORK%/}/umockdev.*/ioctl/_default"
    esandbox disallow_net --connect "unix:${TEMP%/}/umockdev.*/ioctl/_default"

    esandbox disallow_net --bind "unix:${WORK%/}/umockdev.*/ioctl/dev/*"
    esandbox disallow_net --bind "unix:${TEMP%/}/umockdev.*/ioctl/dev/*"
    esandbox disallow_net --connect "unix:${WORK%/}/umockdev.*/ioctl/dev/*"
    esandbox disallow_net --connect "unix:${TEMP%/}/umockdev.*/ioctl/dev/*"
}

