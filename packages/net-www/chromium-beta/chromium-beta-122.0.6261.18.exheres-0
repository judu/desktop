# Copyright 2009-2011 Elias Pipping <pipping@exherbo.org>
# Copyright 2009 Heiko Przybyl <zuxez@cs.tu-berlin.de>
# Copyright 2018 Rasmus Thomsen <cogitri@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require chromium

DESCRIPTION+="
Chromium has three release channels: Stable, Beta, and Dev. This package corresponds to the Beta
channel.
"

PLATFORMS="~amd64 ~x86"

RESTRICT="test"

DEPENDENCIES+="
    build:
    build+run:
        !net-www/chromium-stable-flash-plugin [[
            description = [ Chrome binary plugins don't contain flash anymore ]
            resolution = uninstall-blocked-before
        ]]
        !net-www/chromium-stable-pdf-plugin [[
            description = [ Chromium now provides libpdf ]
            resolution = uninstall-blocked-before
        ]]
    suggestion:
        net-www/chromium-beta-widevine-plugin [[ description = [ Content Decryption Module plugin required for e.g. Netflix ] ]]
"

# Overview with URLs for distro-specific patches
# https://chromium.googlesource.com/chromium/src/+/master/docs/linux_chromium_packages.md
DEFAULT_SRC_PREPARE_PATCHES=(
    "${FILES}"/chromium-widevine.patch
    "${FILES}"/chromium-remove-no-canonical-prefixes.patch
    "${FILES}"/chromium-exherbo-clang-lib-paths.patch
    "${FILES}"/chromium-add-readelf-to-custom-toolchain.patch
    "${FILES}"/chromium-build-remove-cflags-requiring-clang-18.patch
    "${FILES}"/chromium-gl_defines_fix.patch
    "${FILES}"/chromium-unbundle-zlib.patch
    "${FILES}"/chromium-webrtc-add-missing-include.patch
    "${FILES}"/chromium-fix-opus-unbundle-gn.patch
    "${FILES}"/chromium-fix-sandbox-with-glibc-2.34.patch
    "${FILES}"/chromium-allow-shim-headers-in-official-build.patch
    "${FILES}"/chromium-fix-some-page-crashes.patch
    "${FILES}"/chromium-browser-add-missing-typename.patch
    "${FILES}"/chromium-system-minizip.patch
    "${FILES}"/chromium-optimization_guide-missing-typename.patch
    "${FILES}"/chromium-blink-renderer-add-missing-typename.patch
    "${FILES}"/chromium-rustc-remove-z-options.patch
    "${FILES}"/chromium-revert-xslt_processor-changes.patch
)

CHROMIUM_KEEPLIBS=(
    base/third_party/cityhash
    base/third_party/cityhash_v103
    base/third_party/dynamic_annotations
    base/third_party/icu
    base/third_party/nspr
    base/third_party/superfasthash
    base/third_party/symbolize
    base/third_party/valgrind
    base/third_party/xdg_user_dirs
    buildtools/third_party/libc++
    buildtools/third_party/libc++abi
    chrome/third_party/mozilla_security_manager
    courgette/third_party
    net/third_party/mozilla_security_manager
    net/third_party/nss
    net/third_party/quiche
    net/third_party/uri_template
    third_party/angle
    third_party/angle/src/common/third_party/xxhash
    third_party/angle/src/third_party/ceval
    third_party/angle/src/third_party/libXNVCtrl
    third_party/angle/src/third_party/volk
    third_party/anonymous_tokens
    third_party/apple_apsl
    third_party/axe-core
    third_party/blink
    third_party/boringssl
    third_party/boringssl/src/third_party/fiat
    third_party/breakpad
    third_party/breakpad/breakpad/src/third_party/curl
    third_party/catapult
    third_party/catapult/common/py_vulcanize/third_party/rcssmin
    third_party/catapult/common/py_vulcanize/third_party/rjsmin
    third_party/catapult/third_party/polymer
    third_party/catapult/tracing/third_party/d3
    third_party/catapult/tracing/third_party/gl-matrix
    third_party/catapult/tracing/third_party/jpeg-js
    third_party/catapult/tracing/third_party/jszip
    third_party/catapult/tracing/third_party/mannwhitneyu
    third_party/catapult/tracing/third_party/oboe
    third_party/catapult/tracing/third_party/pako
    third_party/ced
    third_party/cld_3
    third_party/closure_compiler
    third_party/content_analysis_sdk
    third_party/cpuinfo
    third_party/crashpad
    third_party/crashpad/crashpad/third_party/lss
    third_party/crashpad/crashpad/third_party/zlib
    third_party/crc32c
    third_party/cros_system_api
    third_party/d3
    third_party/dawn
    third_party/dawn/third_party/gn
    third_party/dawn/third_party/khronos
    third_party/depot_tools
    third_party/devscripts
    third_party/devtools-frontend/
    third_party/devtools-frontend/src/front_end/third_party/acorn
    third_party/devtools-frontend/src/front_end/third_party/additional_readme_paths.json
    third_party/devtools-frontend/src/front_end/third_party/axe-core
    third_party/devtools-frontend/src/front_end/third_party/chromium
    third_party/devtools-frontend/src/front_end/third_party/codemirror
    third_party/devtools-frontend/src/front_end/third_party/csp_evaluator
    third_party/devtools-frontend/src/front_end/third_party/diff
    third_party/devtools-frontend/src/front_end/third_party/i18n
    third_party/devtools-frontend/src/front_end/third_party/intl-messageformat
    third_party/devtools-frontend/src/front_end/third_party/lighthouse
    third_party/devtools-frontend/src/front_end/third_party/lit
    third_party/devtools-frontend/src/front_end/third_party/lodash-isequal
    third_party/devtools-frontend/src/front_end/third_party/marked
    third_party/devtools-frontend/src/front_end/third_party/puppeteer
    third_party/devtools-frontend/src/front_end/third_party/puppeteer/package/lib/esm/third_party/mitt
    third_party/devtools-frontend/src/front_end/third_party/puppeteer/package/lib/esm/third_party/rxjs
    third_party/devtools-frontend/src/front_end/third_party/vscode.web-custom-data
    third_party/devtools-frontend/src/front_end/third_party/wasmparser
    third_party/devtools-frontend/src/test/unittests/front_end/third_party/i18n
    third_party/devtools-frontend/src/third_party/i18n
    third_party/devtools-frontend/src/third_party/pyjson5/src/json5
    third_party/devtools-frontend/src/third_party/typescript
    third_party/distributed_point_functions
    third_party/dom_distiller_js
    third_party/eigen3
    third_party/emoji-segmenter
    third_party/farmhash
    third_party/fdlibm
    third_party/fft2d
    third_party/flatbuffers
    third_party/fp16
    third_party/fusejs
    third_party/fxdiv
    third_party/gemmlowp
    third_party/google_input_tools
    third_party/google_input_tools/third_party/closure_library
    third_party/google_input_tools/third_party/closure_library/third_party/closure
    third_party/googletest
    third_party/highway
    third_party/hunspell
    third_party/iccjpeg
    third_party/ipcz
    third_party/inspector_protocol
    third_party/jinja2
    third_party/jstemplate
    third_party/khronos
    third_party/leveldatabase
    third_party/libaddressinput
    third_party/libaom
    third_party/libaom/source/libaom/third_party/fastfeat
    third_party/libaom/source/libaom/third_party/SVT-AV1
    third_party/libaom/source/libaom/third_party/vector
    third_party/libaom/source/libaom/third_party/x86inc
    third_party/libavifinfo
    third_party/libc++
    third_party/libc++abi
    third_party/libgav1
    third_party/libjingle_xmpp
    third_party/libphonenumber
    third_party/libsecret
    third_party/libsrtp
    third_party/libsync
    third_party/libudev
    third_party/liburlpattern
    third_party/libva_protected_content
    third_party/libwebm
    third_party/libx11
    third_party/libxcb-keysyms
    third_party/libxml
    third_party/libxml/chromium
    third_party/libyuv
    third_party/libzip
    third_party/lit
    third_party/lottie
    third_party/llvm
    third_party/lss
    third_party/lzma_sdk
    third_party/mako
    third_party/maldoca
    third_party/maldoca/src/third_party/tensorflow_protos
    third_party/maldoca/src/third_party/zlibwrapper
    third_party/material_color_utilities
    third_party/markupsafe
    third_party/metrics_proto
    third_party/minigbm
    third_party/modp_b64
    third_party/nasm
    third_party/nearby
    third_party/neon_2_sse
    third_party/node
    third_party/omnibox_proto
    third_party/one_euro_filter
    third_party/openscreen
    third_party/openscreen/src/third_party/tinycbor
    third_party/ots
    third_party/perfetto
    third_party/perfetto/protos/third_party/chromium
    third_party/pdfium
    third_party/pdfium/third_party/agg23
    third_party/pdfium/third_party/base
    third_party/pdfium/third_party/bigint
    third_party/pdfium/third_party/freetype
    third_party/pdfium/third_party/lcms
    third_party/pdfium/third_party/libopenjpeg
    third_party/pdfium/third_party/libtiff
    third_party/pffft
    third_party/ply
    third_party/polymer
    third_party/private-join-and-compute
    third_party/private_membership
    third_party/protobuf
    third_party/pthreadpool
    third_party/puffin
    third_party/pyjson5/src/json5
    third_party/pyyaml
    third_party/qcms
    third_party/rnnoise
    third_party/ruy
    third_party/rust
    third_party/s2cellid
    third_party/securemessage
    third_party/shell-encryption
    third_party/skia
    third_party/skia/include/third_party/vulkan/vulkan
    third_party/skia/third_party/vulkanmemoryallocator
    third_party/smhasher
    third_party/sqlite
    third_party/swiftshader
    third_party/swiftshader/third_party/astc-encoder
    third_party/swiftshader/third_party/llvm-subzero
    third_party/swiftshader/third_party/marl
    third_party/swiftshader/third_party/subzero
    third_party/tensorflow_models
    third_party/tensorflow-text
    third_party/tflite
    third_party/tflite/src/third_party/eigen3
    third_party/tflite/src/third_party/fft2d
    third_party/tflite/src/third_party/xla
    third_party/tflite/src/third_party/xla/third_party/tsl
    third_party/tflite_support
    third_party/ukey2
    third_party/unrar
    third_party/utf
    third_party/vulkan-deps/glslang
    third_party/vulkan-deps/vulkan-headers
    third_party/vulkan-deps/vulkan-loader
    third_party/vulkan-deps/vulkan-tools
    third_party/vulkan-deps/vulkan-utility-libraries
    third_party/vulkan-deps/vulkan-validation-layers
    third_party/vulkan_memory_allocator
    third_party/wayland
    third_party/wayland-protocols
    third_party/webdriver
    third_party/webgpu-cts
    third_party/webrtc
    third_party/webrtc/common_audio/third_party/ooura
    third_party/webrtc/common_audio/third_party/spl_sqrt_floor
    third_party/webrtc/modules/third_party/fft
    third_party/webrtc/modules/third_party/g711
    third_party/webrtc/modules/third_party/g722
    third_party/webrtc/rtc_base/third_party/base64
    third_party/webrtc/rtc_base/third_party/sigslot
    third_party/widevine
    third_party/wuffs
    third_party/x11proto
    third_party/xcbproto
    third_party/xnnpack
    third_party/zlib/google
    third_party/zstd
    third_party/zxcvbn-cpp
    url/third_party/mozilla
    v8/src/third_party/siphash
    v8/src/third_party/utf8-decoder
    v8/src/third_party/valgrind
    v8/third_party/glibc
    v8/third_party/inspector_protocol
    v8/third_party/v8
)

# TODO: keep for GN builds
CHROMIUM_KEEPLIBS+=(
    third_party/speech-dispatcher
    third_party/usb_ids
    third_party/xdg-utils
    tools/gn/src/base/third_party/icu
)

# dependencies that could be unbundled if not for build errors
CHROMIUM_KEEPLIBS+=(
    # A whole bunch of Unresolved dependencies, eg
    # //third_party/abseil-cpp:absl_component_deps(//build/toolchain/linux:clang_x64)
    #  needs //third_party/abseil-cpp/absl/base:nullability(//build/toolchain/linux:clang_x64)
    # //third_party/abseil-cpp:absl_component_deps(//build/toolchain/linux:clang_x64)
    #  needs //third_party/abseil-cpp/absl/base:prefetch(//build/toolchain/linux:clang_x64)
    # //third_party/abseil-cpp:absl_component_deps(//build/toolchain/linux:clang_x64)
    #  needs //third_party/abseil-cpp/absl/log:absl_check(//build/toolchain/linux:clang_x64)
    # //third_party/abseil-cpp:absl_component_deps(//build/toolchain/linux:clang_x64)
    #  needs //third_party/abseil-cpp/absl/log:absl_log(//build/toolchain/linux:clang_x64)
    # //third_party/abseil-cpp:absl_component_deps(//build/toolchain/linux:clang_x64)
    #  needs //third_party/abseil-cpp/absl/log:die_if_null(//build/toolchain/linux:clang_x64)
    # //third_party/abseil-cpp:absl_component_deps(//build/toolchain/linux:clang_x64)
    #  needs //third_party/abseil-cpp/absl/random:distributions(//build/toolchain/linux:clang_x64)
    # //third_party/abseil-cpp:absl_component_deps(//build/toolchain/linux:clang_x64)
    #  needs //third_party/abseil-cpp/absl/strings:string_view(//build/toolchain/linux:clang_x64)
    third_party/abseil-cpp

    # requires headers from include/freetype/internal/ that aren't part of our package
    third_party/freetype

    # ld.lld: error: undefined symbol: Json::Value::Value(std::__Cr::basic_string<char, std::__Cr::char_traits<char>, std::__Cr::allocator<char>> const&)
    # ld.lld: error: undefined symbol: Json::Value::operator[](std::__Cr::basic_string<char, std::__Cr::char_traits<char>, std::__Cr::allocator<char>> const&)
    # ld.lld: error: undefined symbol: Json::Value::toStyledString() const
    # ld.lld: error: undefined symbol: Json::Value::getMemberNames() const
    # ld.lld: error: undefined symbol: Json::Value::asString() const
    # ld.lld: error: undefined symbol: Json::Value::operator[](std::__Cr::basic_string<char, std::__Cr::char_traits<char>, std::__Cr::allocator<char>> const&) const
    third_party/jsoncpp

    # requires some unreleased changes
    # error: no member named 'imageSequenceTrackPresent' in 'avifDecoder'
    # error: no member named 'enableParsingGainMapMetadata' in 'avifDecoder'
    # error: no member named 'gainMapPresent' in 'avifDecoder'
    # error: unknown type name 'avifGainMapMetadata'
    # error: no member named 'gainMap' in 'avifImage'
    third_party/libavif

    # //media/gpu:common(//build/toolchain/linux/unbundle:default)
    #   needs //third_party/libvpx:libvpx_config(//build/toolchain/linux/unbundle:default)
    # //media/gpu:common(//build/toolchain/linux/unbundle:default)
    #   needs //third_party/libvpx:libvpxrc(//build/toolchain/linux/unbundle:default)
    # //media/gpu/vaapi:unit_test(//build/toolchain/linux/unbundle:default)
    #   needs //third_party/libvpx:libvpx_config(//build/toolchain/linux/unbundle:default)
    # //media/gpu/vaapi:unit_test(//build/toolchain/linux/unbundle:default)
    #   needs //third_party/libvpx:libvpxrc(//build/toolchain/linux/unbundle:default)
    # //media/gpu/vaapi:vaapi(//build/toolchain/linux/unbundle:default)
    #   needs //third_party/libvpx:libvpx_config(//build/toolchain/linux/unbundle:default)
    # //media/gpu/vaapi:vaapi(//build/toolchain/linux/unbundle:default)
    #   needs //third_party/libvpx:libvpxrc(//build/toolchain/linux/unbundle:default)
    third_party/libvpx
    third_party/libvpx/source/libvpx/third_party/x86inc

    # Usage of the system libre2.so is not supported with use_custom_libcxx=true because the library's interface relies on libstdc++'s std::string and std::vector.
    third_party/re2

    # ld.lld: error: undefined symbol: snappy::Compress(char const*, unsigned long, std::__Cr::basic_string<char, std::__Cr::char_traits<char>, std::__Cr::allocator<char>>*)
    third_party/snappy

    # ld.lld: error: undefined symbol: spvtools::Optimizer::SetMessageConsumer(std::__Cr::function<void (spv_message_level_t, char const*, spv_position_t const&, char const*)>)
    # ld.lld: error: undefined symbol: spvtools::CreateSetSpecConstantDefaultValuePass(std::__Cr::unordered_map<unsigned int, std::__Cr::vector<unsigned int, std::__Cr::allocator<unsigned int>>, std::__Cr::hash<unsigned int>, std::__Cr::equal_to<unsigned int>, std::__Cr::allocator<std::__Cr::pair<unsigned int const, std::__Cr::vector<unsigned int, std::__Cr::allocator<unsigned int>>>>> const&)
    # ld.lld: error: undefined symbol: spvtools::Optimizer::Run(unsigned int const*, unsigned long, std::__Cr::vector<unsigned int, std::__Cr::allocator<unsigned int>>*, spvtools::ValidatorOptions const&, bool) const
    # ld.lld: error: undefined symbol: spvtools::Optimizer::Run(unsigned int const*, unsigned long, std::__Cr::vector<unsigned int, std::__Cr::allocator<unsigned int>>*, spv_optimizer_options_t*) const
    third_party/swiftshader/third_party/SPIRV-Headers
    third_party/swiftshader/third_party/SPIRV-Tools
    third_party/vulkan-deps/spirv-headers
    third_party/vulkan-deps/spirv-tools

    # ld.lld: error: undefined symbol: woff2::WOFF2StringOut::WOFF2StringOut
    third_party/woff2
)

CHROMIUM_SYSTEM_LIBS="
    brotli
    dav1d
    double-conversion
    ffmpeg
    flac
    fontconfig
    harfbuzz-ng
    icu
    libdrm
    libevent
    libjpeg
    libpng
    libwebp
    libxml
    libxslt
    openh264
    opus
    zlib
"

