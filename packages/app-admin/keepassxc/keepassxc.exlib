# Copyright 2017-2023 Timo Gurr <tgurr@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require github [ user=keepassxreboot release=${PV} pnv=${PNV}-src suffix=tar.xz ] \
    cmake \
    freedesktop-desktop \
    freedesktop-mime \
    gtk-icon-cache \
    utf8-locale

export_exlib_phases src_test pkg_postinst pkg_postrm

SUMMARY="Cross-platform community driven port of the windows application KeePass"
DESCRIPTION="
KeePassXC manages user names, passwords, URLs, attachments and comments in a single database
encrypted with AES or Twofish. The database format is compatible with the original KeePass Password
Safe. Additional features compared to KeePassX:

* Secure storage of passwords and other private data with AES, Twofish or ChaCha20 encryption
* File format compatibility with KeePass2, KeePassX, MacPass, KeeWeb and many others (KDBX 3.1 and 4.0)
* SSH Agent integration
* Auto-Type on all supported platforms for automagically filling in login forms
* Key file and YubiKey challenge-response support for additional security
* TOTP generation (including Steam Guard and QR Codes)
* CSV import from other password managers (e.g., LastPass)
* Command line interface
* Stand-alone password and passphrase generator
* Password strength meter
* Custom icons for database entries and download of website favicons
* Database merge functionality
* Automatic reload when the database was changed externally
* Optional Browser integration with KeePassXC-Browser using native messaging for Mozilla Firefox,
  Google Chrome, Chromium and Vivaldi (https://keepassxc.org/docs/keepassxc-browser-migration)
* KeeShare database sharing and synchronization
"

LICENCES="|| ( GPL-2 GPL-3 ) BSD-2 BSD-3 GPL-2 LGPL-2.1 LGPL-3 CC0 public-domain OFL-1.1 || ( LGPL-2.1 GPL-3 )"
SLOT="0"
MYOPTIONS="
    openmp
    yubikey [[ description = [ YubiKey challenge-response support for enhancing the encryption key of the database ] ]]
    wayland
"

QT_MIN_VER=5.2.0

DEPENDENCIES="
    build:
        app-text/asciidoctor
        x11-libs/qttools:5[>=${QT_MIN_VER}]
    build+run:
        app-crypt/argon2
        dev-libs/botan:=[>=2.11.0]
        media-libs/qrencode:4
        sys-libs/readline:=
        sys-libs/zlib[>=1.2.0]
        x11-libs/libX11
        x11-libs/libXi [[ description = [ needed for auto-type ] ]]
        x11-libs/libXtst [[ description = [ needed for auto-type ] ]]
        x11-libs/qtbase:5[>=${QT_MIN_VER}][gui]
        x11-libs/qtsvg:5[>=${QT_MIN_VER}]
        x11-libs/qtx11extras:5[>=${QT_MIN_VER}] [[ description = [ needed for auto-type ] ]]
        openmp? ( sys-libs/libgomp:= )
        yubikey? ( sys-apps/pcsc-lite )
    run:
        wayland? ( x11-libs/qtwayland:5 )
"

CMAKE_SOURCE=${WORKBASE}/${PNV}

CMAKE_SRC_CONFIGURE_PARAMS=(
    -DKEEPASSXC_BUILD_TYPE:STRING="Release"
    -DPRINT_SUMMARY:BOOL=TRUE
    -DWITH_ASAN:BOOL=FALSE
    -DWITH_CCACHE:BOOL=FALSE
    -DWITH_COVERAGE:BOOL=FALSE
    -DWITH_DEV_BUILD:BOOL=FALSE
    -DWITH_GUI_TESTS:BOOL=FALSE
    -DWITH_XC_AUTOTYPE:BOOL=TRUE
    -DWITH_XC_BOTAN3:BOOL=FALSE
    -DWITH_XC_BROWSER:BOOL=TRUE
    -DWITH_XC_DOCS:BOOL=TRUE
    -DWITH_XC_FDOSECRETS:BOOL=TRUE
    -DWITH_XC_KEESHARE:BOOL=TRUE
    -DWITH_XC_NETWORKING:BOOL=TRUE
    -DWITH_XC_SSHAGENT:BOOL=TRUE
    -DWITH_XC_UPDATECHECK:BOOL=FALSE
    -DWITH_XC_X11:BOOL=TRUE
)
CMAKE_SRC_CONFIGURE_OPTION_DISABLE_FINDS=(
    'openmp OpenMP'
)
CMAKE_SRC_CONFIGURE_OPTION_WITHS=(
    "yubikey XC_YUBIKEY"
)
CMAKE_SRC_CONFIGURE_TESTS=(
    '-DWITH_TESTS:BOOL=TRUE -DWITH_TESTS:BOOL=FALSE'
)

DEFAULT_SRC_TEST_PARAMS=(
    ARGS="--verbose --exclude-regex '^(testcli|testsshagent)'"
)

keepassxc_src_test() {
    # UTF8 required for test # 19
    # https://github.com/keepassxreboot/keepassxc/issues/667
    require_utf8_locale

    default
}

keepassxc_pkg_postinst() {
    freedesktop-desktop_pkg_postinst
    freedesktop-mime_pkg_postinst
    gtk-icon-cache_pkg_postinst
}

keepassxc_pkg_postrm() {
    freedesktop-desktop_pkg_postrm
    freedesktop-mime_pkg_postrm
    gtk-icon-cache_pkg_postrm
}

